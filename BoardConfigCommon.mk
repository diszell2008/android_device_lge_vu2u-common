#
# Copyright (C) 2013 The CyanogenMod Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

TARGET_GLOBAL_CFLAGS += -mfpu=neon -mfloat-abi=softfp
TARGET_GLOBAL_CPPFLAGS += -mfpu=neon -mfloat-abi=softfp
TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_CPU_SMP := true
TARGET_ARCH := arm
TARGET_ARCH_VARIANT := armv7-a-neon
TARGET_CPU_VARIANT := krait
TARGET_BOARD_PLATFORM := msm8960

# Krait optimizations
TARGET_USE_KRAIT_BIONIC_OPTIMIZATION := true
TARGET_USE_KRAIT_PLD_SET := true
TARGET_KRAIT_BIONIC_PLDOFFS := 10
TARGET_KRAIT_BIONIC_PLDTHRESH := 10
TARGET_KRAIT_BIONIC_BBTHRESH := 64
TARGET_KRAIT_BIONIC_PLDSIZE := 64
TARGET_NO_BOOTLOADER := true

#Audio
BOARD_USES_ALSA_AUDIO := true
BOARD_USES_LEGACY_ALSA_AUDIO := true
TARGET_USES_QCOM_COMPRESSED_AUDIO := true
QCOM_ANC_HEADSET_ENABLED := false
QCOM_FLUENCE_ENABLED := false

#Kernel
TARGET_KERNEL_SOURCE := kernel/lge/vu2ulsk
BOARD_KERNEL_BASE := 0x80200000
BOARD_KERNEL_PAGESIZE := 2048
BOARD_MKBOOTIMG_ARGS := --ramdisk_offset 0x02000000
BOARD_KERNEL_CMDLINE := vmalloc=600M  console=ttyHSL0,115200,n8 androidboot.hardware=fx1sk user_debug=31 msm_rtb.filter=0x3F ehci-hcd.park=3 maxcpus=2 lpj=67741
TARGET_KERNEL_CONFIG := vu2kt-perf_defconfig
COMMON_GLOBAL_CFLAGS += -DBOARD_CHARGING_CMDLINE_NAME='"androidboot.mode"' -DBOARD_CHARGING_CMDLINE_VALUE='"chargerlogo"'

#Bluetooth
BOARD_HAVE_BLUETOOTH := true

TARGET_NO_RADIOIMAGE := true

#EGL
BOARD_EGL_CFG := device/lge/vu2u-common/egl.cfg
TARGET_DISPLAY_INSECURE_MM_HEAP := true
USE_OPENGL_RENDERER := true
TARGET_USES_ION := true
TARGET_USES_OVERLAY := true
TARGET_USES_SF_BYPASS := true
TARGET_USES_C2D_COMPOSITION := false
NUM_FRAMEBUFFER_SURFACE_BUFFERS := 3

#Media
TARGET_NO_ADAPTIVE_PLAYBACK := true

# Recovery
BOARD_USE_CUSTOM_RECOVERY_FONT := \"roboto_15x24.h\"
RECOVERY_FSTAB_VERSION = 2

#Partition
TARGET_USERIMAGES_USE_EXT4 := true
BOARD_BOOTIMAGE_PARTITION_SIZE := 12582912
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 12582912
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 1798307840
BOARD_USERDATAIMAGE_PARTITION_SIZE := 4404019200
BOARD_CACHEIMAGE_PARTITION_SIZE := 826277888
BOARD_PERSISTIMAGE_PARTITION_SIZE := 5127433
BOARD_FLASH_BLOCK_SIZE := 131072
BOARD_USES_SECURE_SERVICES := true

#Thermal
BOARD_USES_EXTRA_THERMAL_SENSOR := true

#GPS
BOARD_VENDOR_QCOM_GPS_LOC_API_HARDWARE := $(TARGET_BOARD_PLATFORM)
TARGET_NO_RPC := true

BOARD_CHARGER_ENABLE_SUSPEND := true
BOARD_HAS_NO_SELECT_BUTTON := true

#Camera
USE_DEVICE_SPECIFIC_CAMERA := true
TARGET_ENABLE_QC_AV_ENHANCEMENTS := true
COMMON_GLOBAL_CFLAGS += -DLG_CAMERA_HARDWARE

#QCOM
BOARD_USES_QCOM_HARDWARE := true
TARGET_USES_QCOM_BSP := true
BOARD_USES_QCOM_LIBS := true

#Ril
BOARD_RIL_CLASS := ../../../device/lge/vu2u-common/ril/

#CMHW
BOARD_HARDWARE_CLASS := device/lge/vu2u-common/cmhw/

# Power HAL
TARGET_POWERHAL_VARIANT := qcom

# SELinux policies
# qcom sepolicy
include device/qcom/sepolicy/sepolicy.mk

# Common gproj policies
BOARD_SEPOLICY_DIRS += \
        device/lge/vu2u-common/sepolicy

TARGET_USES_LOGD := false
BOARD_USES_LEGACY_MMAP := true
EXTENDED_FONT_FOOTPRINT := true
